﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tankstation
{
    public class Brændstof
    {
        private double _brændstofPris = 9.95;
        private double _brændstofLager = 1000;

        public double GetPrice
        {
            get
            {
                return _brændstofPris;
            }
        }

        public double GetGasAmount
        {
            get
            {
                return _brændstofLager;
            }
            set
            {
                if(value > 0)
                {
                   _brændstofLager = value + _brændstofLager;
                }
                else if (value < 0)
                {
                    _brændstofLager = _brændstofLager - value;
                }

                else
                {

                }
            }
        }
    }
}